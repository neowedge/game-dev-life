﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamingArts.GameDevLife
{
    public class FunsManager : MonoBehaviour
    {
        [ShowInInspector, HideInEditorMode]
        private Dictionary<Fun, FunController> _funControllers;

        GameManager _gameManager;

        public GameManager GameManager { get => _gameManager; }

        public IEnumerator<FunController> GetEnumerator()
        {
            return _funControllers.Values.GetEnumerator();
        }

        public void Setup(GameManager gameManager)
        {
            _gameManager = gameManager;

            Fun[] funList = Resources.LoadAll<Fun>("Fun");
            _funControllers = new Dictionary<Fun, FunController>(funList.Length);
            for (int i = 0; i< funList.Length; ++i)
            {
                _funControllers.Add(funList[i], new FunController(funList[i], this));
            }
        }

        public void Init()
        {
            IEnumerator<FunController> e = _funControllers.Values.GetEnumerator();
            while (e.MoveNext())
            {
                e.Current.Init();
            }
        }

        private void OnDestroy()
        {
            if (_funControllers != null)
            {
                IEnumerator<FunController> e = _funControllers.Values.GetEnumerator();
                while (e.MoveNext())
                {
                    e.Current.Dispose();
                }
            }
        }
    }
}
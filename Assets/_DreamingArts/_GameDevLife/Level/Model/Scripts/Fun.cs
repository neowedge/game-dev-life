﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamingArts.GameDevLife
{
    [CreateAssetMenu(menuName = "[Game Dev Life] Fun")]
    public class Fun : ScriptableObject
    {
        [SerializeField]
        private string _name;

        [SerializeField]
        private string _description;

        [SerializeField]
        private string _deathPlace;

        [SerializeField]
        private int _moneyRequired;

        [SerializeField]
        private int _lifeCost;

        [SerializeField]
        private int _happinessReward;

        public string Name { get => _name; }
        public string Description { get => _description; }
        public string DeathPlace { get => _deathPlace; }
        public int MoneyRequired { get => _moneyRequired; }
        public int LifeCost { get => _lifeCost; }
        public int HappinessReward { get => _happinessReward; }
    }
}
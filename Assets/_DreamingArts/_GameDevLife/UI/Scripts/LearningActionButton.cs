﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DreamingArts.GameDevLife
{
    public class LearningActionButton : ButtonDescription
    {
        [SerializeField]
        TMP_Text _buttonText;

        LearningController _action;

        private void OnDestroy()
        {
            if (_action != null)
            {
                _action.OnUnlocked -= Action_OnUnlocked;
            }
        }

        private void SetCompleteText()
        {
            string classes = _action.Level < 24 ?
                    _action.Level + " months" :
                    Mathf.FloorToInt(_action.Level / 12) + " years";
            _menuPanel.SetCompleteDescriptionText(string.Format(
                    _menuPanel.LearningCompleteDescriptionTemplate,
                    _action.Learning.Name,
                    GameManager.FormatLifeCost(_action.Learning.LifeCost),
                    classes));
        }

        public void Setup(LearningController action, MenuPanel menuPanel)
        {
            _action = action;
            _buttonText.text = _action.Learning.Name;
            _description = _action.Learning.Description;
            base.Setup(menuPanel);

            if (!_action.Unlocked)
            {
                gameObject.SetActive(false);
                _action.OnUnlocked += Action_OnUnlocked;
            }
        }

        private void Action_OnUnlocked()
        {
            _action.OnUnlocked -= Action_OnUnlocked;
            gameObject.SetActive(true);
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            base.OnPointerEnter(eventData);
            SetCompleteText();
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            base.OnPointerExit(eventData);
            _menuPanel.UnsetCompleteDescriptionText();
        }

        public void LevelUp()
        {
            _action.LevelUp();
            SetCompleteText();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamingArts.GameDevLife
{
    public class LearningController : LifeActionController
    {
        Learning _learning;
        LearningsManager _context;

        public Learning Learning { get => _learning; }
        public LearningsManager Context { get => _context; }

        public static event System.Action<LearningController> OnAnyLevelUp;

        public LearningController(Learning learning, LearningsManager context)
        {
            _learning = learning;
            _context = context;
        }

        public void Init()
        {
            Unlocked = IsUnlocked();
            if (!Unlocked)
            {
                if (_context.GameManager.Age < _learning.AgeRequired)
                {
                    _context.GameManager.OnLifeAction += GameManager_OnLifeAction;
                }
                else
                {
                    LearningController.OnAnyLevelUp += Learning_OnAnyLevelUp;
                }
            }
        }

        private void GameManager_OnLifeAction()
        {
            if (_context.GameManager.Age >= _learning.AgeRequired)
            {
                _context.GameManager.OnLifeAction -= GameManager_OnLifeAction;
                Unlocked = IsUnlocked();
                if (!Unlocked)
                {
                    LearningController.OnAnyLevelUp += Learning_OnAnyLevelUp;
                }
            }
        }

        protected override bool IsUnlocked()
        {
            if (_context.GameManager.Age < _learning.AgeRequired)
            {
                return false;
            }
            foreach (KeyValuePair<Learning, int> requirement in _learning.LearningRequirements)
            {
                if (_context.Get(requirement.Key).Level < requirement.Value)
                {
                    return false;
                }
            }

            return true;
        }

        public override void LevelUp()
        {
            base.LevelUp();
            _context.GameManager.MakeLifeAction(this, -_learning.LifeCost, -_learning.HappinessCost, 0);
            OnAnyLevelUp?.Invoke(this);
        }

        private void Learning_OnAnyLevelUp(LearningController learning)
        {
            if (IsUnlocked())
            {
                Unlocked = true;
                LearningController.OnAnyLevelUp -= Learning_OnAnyLevelUp;
            }
        }

        public void Dispose()
        {
            if (_context != null && _context.GameManager != null)
            {
                _context.GameManager.OnLifeAction -= GameManager_OnLifeAction;
            }
            LearningController.OnAnyLevelUp -= Learning_OnAnyLevelUp;
        }
    }
}
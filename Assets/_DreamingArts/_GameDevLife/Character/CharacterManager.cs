﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamingArts.GameDevLife
{
    public class CharacterManager : SerializedMonoBehaviour
    {
        [SerializeField]
        Dictionary<int, Transform> _ageCharacter;

        Transform _currentCharacter;

        public Transform CurrentCharacter { get => _currentCharacter; }

        public Transform CreateCharacter(Vector3 position)
        {
            _currentCharacter = Instantiate(_ageCharacter[0], position, Quaternion.identity);
            return _currentCharacter;
        }
    }
}

/*
 * StylePreset
 * Copyright (c) Koron Studios
 * http://www.koronstudios.com/
 *
 * Created on 2/8/2018 13:18:16
 */

using Sirenix.OdinInspector;
using UnityEngine;

namespace DreamingArts
{

    /// <summary>
    /// Describe your class
    /// </summary>
    [CreateAssetMenu(menuName = "[Dreaming Arts] UI/Style Preset")]
    public class StylePreset : ScriptableObject
    {

        #region Types

        #endregion

        #region Unity Editor Fields
        [SerializeField]
        internal Color _bottomColor = Color.white;

        [SerializeField]
        private Color _strokeColor = Color.white;

        [SerializeField]
        private Color _baseColor = Color.white;

        [SerializeField]
        private Color _baseTextColor = Color.white;

        [SerializeField]
        private Color _innerPanelColor = Color.white;

        [SerializeField]
        private Color _innerPanelTextColor = Color.white;

        #endregion

        #region Private Fields

        #endregion

        #region Public Properties
        public Color BottomColor {
            get {
                return _bottomColor;
            }
#if UNITY_EDITOR
            internal set {
                _bottomColor = value;
                UnityEditor.EditorUtility.SetDirty(this);
            }
#endif
        }

        public Color StrokeColor {
            get {
                return _strokeColor;
            }
#if UNITY_EDITOR
            internal set {
                _strokeColor = value;
                UnityEditor.EditorUtility.SetDirty(this);
            }
#endif
        }

        public Color BaseColor {
            get {
                return _baseColor;
            }
#if UNITY_EDITOR
            internal set {
                _baseColor = value;
                UnityEditor.EditorUtility.SetDirty(this);
            }
#endif
        }

        public Color BaseTextColor {
            get {
                return _baseTextColor;
            }
#if UNITY_EDITOR
            internal set {
                _baseTextColor = value;
                UnityEditor.EditorUtility.SetDirty(this);
            }
#endif
        }

        public Color InnerPanelColor {
            get {
                return _innerPanelColor;
            }
#if UNITY_EDITOR
            internal set {
                _innerPanelColor = value;
                UnityEditor.EditorUtility.SetDirty(this);
            }
#endif
        }

        public Color InnerPanelTextColor {
            get {
                return _innerPanelTextColor;
            }
#if UNITY_EDITOR
            internal set {
                _innerPanelTextColor = value;
                UnityEditor.EditorUtility.SetDirty(this);
            }
#endif
        }

        #endregion
    }
}
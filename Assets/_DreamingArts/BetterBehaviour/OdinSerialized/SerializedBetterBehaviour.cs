using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamingArts {

    /// <author>Miguel López Ramírez</author>
    /// <summary>
    /// Improved MonoBehaviour. It is recommended to use a custom class that inherits from MonoBehavior in order to add our own features and optimizations.
    /// </summary>
    public abstract class SerializedBetterBehaviour : SerializedMonoBehaviour {

        Transform _transform = null;
        RectTransform _rectTransform = null;

        /// <summary>
        /// Cached Transform (property «MonoBehaviour.transform» always calls to GetComponent<Transform>(), instead of keep result in memory, so this is a more optimized property).
        /// </summary>
        public Transform Transform {
            get {
                if (_transform == null) {
                    _transform = GetComponent<Transform>();
                }
                return _transform;
            }
        }

        public RectTransform RectTransform {
            get {
                if (_transform != null) {
                    _rectTransform = (RectTransform)_transform;
                } else if (_rectTransform == null) {
                    _rectTransform = GetComponent<RectTransform>(); ;
                }
                return _rectTransform;
            }
        }
    }
}

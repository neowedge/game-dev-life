﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamingArts.GameDevLife
{
    [SerializeField]
    public class JobsSlot
    {
        [SerializeField]
        private string _slotKey;

        [ShowInInspector, HideInEditorMode]
        private Dictionary<Job, JobController> _jobControllers;

        [ShowInInspector, HideInEditorMode]
        private JobController _currentJob;

        [ShowInInspector, HideInEditorMode]
        private IEnumerator<JobController> _nextJobEnumerator;

        [ShowInInspector, HideInEditorMode]
        bool _isNotLast = false;

        private JobsManager _context;

        public JobsManager Context { get => _context; }
        public JobController CurrentJob { get => _currentJob; }

        public event System.Action OnCurrentJobChange;

        public JobController Get(Job job)
        {
            if (_jobControllers.ContainsKey(job))
            {
                return _jobControllers[job];
            }
            return null;
        }

        public void Setup(JobsManager context)
        {
            _context = context;

            Job[] jobList = Resources.LoadAll<Job>("Job/" + _slotKey);
            _jobControllers = new Dictionary<Job, JobController>(jobList.Length);
            for (int i = 0; i < jobList.Length; ++i)
            {
                _jobControllers.Add(jobList[i], new JobController(jobList[i], this));
            }

            _nextJobEnumerator = _jobControllers.Values.GetEnumerator();

            while (_isNotLast = _nextJobEnumerator.MoveNext())
            {
                if (_nextJobEnumerator.Current.Unlocked)
                {
                    _currentJob = _nextJobEnumerator.Current;
                }
                else
                {
                    break;
                }
            }
        }

        public void Init()
        {
            IEnumerator<JobController> e = _jobControllers.Values.GetEnumerator();
            while (e.MoveNext())
            {
                e.Current.Init();
            }

            if (_nextJobEnumerator.Current == null)
            {
                Debug.Log(_slotKey + " is null");
                return;
            }
            _nextJobEnumerator.Current.OnUnlocked += JobManager_OnUnlocked;
        }

        private void JobManager_OnUnlocked()
        {
            _nextJobEnumerator.Current.OnUnlocked -= JobManager_OnUnlocked;
            _currentJob = _nextJobEnumerator.Current;
            _isNotLast = _nextJobEnumerator.MoveNext();
            if (_isNotLast)
            {
                _nextJobEnumerator.Current.OnUnlocked += JobManager_OnUnlocked;
            }
            OnCurrentJobChange?.Invoke();
        }

        private void OnDestroy()
        {
            IEnumerator<JobController> e = _jobControllers.Values.GetEnumerator();
            while (e.MoveNext())
            {
                e.Current.Dispose();
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DreamingArts.GameDevLife
{
    public class FunActionButton : ButtonDescription
    {
        [SerializeField]
        TMP_Text _buttonText;

        FunController _action;

        private void OnDestroy()
        {
            if (_action != null)
            {
                _action.OnUnlocked -= Action_OnUnlocked;
            }
        }

        public void Setup(FunController action, MenuPanel menuPanel)
        {
            _action = action;
            _buttonText.text = _action.Fun.Name;
            _description = _action.Fun.Description;
            base.Setup(menuPanel);

            if (!_action.Unlocked)
            {
                gameObject.SetActive(false);
                _action.OnUnlocked += Action_OnUnlocked;
            }
        }

        private void SetCompleteText()
        {
            _menuPanel.SetCompleteDescriptionText(string.Format(
                    _menuPanel.FunCompleteDescriptionTemplate,
                    _action.Fun.Name,
                    GameManager.FormatLifeCost(_action.Fun.LifeCost),
                    _action.Fun.MoneyRequired.ToString("#,0"),
                    (int)(_action.Fun.HappinessReward * 100f / _action.Context.GameManager.MaxHappiness)));
        }

        private void Action_OnUnlocked()
        {
            _action.OnUnlocked -= Action_OnUnlocked;
            gameObject.SetActive(true);
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            base.OnPointerEnter(eventData);
            SetCompleteText();
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            base.OnPointerExit(eventData);
            _menuPanel.UnsetCompleteDescriptionText();
        }

        public void LevelUp()
        {
            _action.LevelUp();
            SetCompleteText();
        }
    }
}
﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DreamingArts.GameDevLife
{
    public class GameScreen : MonoBehaviour
    {
        [SerializeField]
        Camera _camera;

        [SerializeField]
        GameManager _gameManager;

        [SerializeField]
        MenuPanel _menuPanel;

        [SerializeField]
        RectTransform _topLeftPanel;

        [SerializeField]
        TMP_Text _ageText;

        [SerializeField]
        Slider _lifeSlider;

        [SerializeField]
        Slider _happinessSlider;

        [SerializeField]
        TMP_Text _moneyText;

        [SerializeField]
        ButtonDescription _independantButton;

        [SerializeField]
        TMP_Text _independantText;

        [SerializeField]
        ButtonDescription _getBackAtYourParentsButton;

        [SerializeField]
        TMP_Text _getBackAtYourParentsText;

        private void Start()
        {
            _independantButton.Setup(_menuPanel);
            _getBackAtYourParentsButton.Setup(_menuPanel);
        }

        public void StartGame()
        {
            DOTween.Sequence()
                    .Append(_menuPanel.Show())
                    .Join(_gameManager.CharacterManager.CurrentCharacter.DOMoveY(3f, 0.2f))
                    .Join(_camera.DOColor(new Color(1f, 0.70196f, 0.913725f), 0.2f))
                    .OnComplete(() =>
                    {
                        _gameManager.StartGame();
                        _lifeSlider.maxValue = _gameManager.StartLife;
                        _happinessSlider.maxValue = _gameManager.MaxHappiness;
                        SetGameValues();
                        
                        _topLeftPanel.gameObject.SetActive(true);
                        _ageText.gameObject.SetActive(true);

                        _gameManager.OnLifeAction += GameManager_OnLifeAction;
                    });
        }

        public void BecameIndependent()
        {
            _gameManager.BecameIndependent();
        }

        public void GetBackAtYourParents()
        {
            _gameManager.GetBackAtYourParents();
        }

        private void GameManager_OnLifeAction()
        {
            SetGameValues();
        }

        private void SetGameValues()
        {
            if (_gameManager.Months < 24)
            {
                _ageText.text = _gameManager.Months + " months old";
            }
            else
            {
                _ageText.text = _gameManager.Age + " years old";
                if (_gameManager.IndependentAge)
                {
                    if (_gameManager.Independent)
                    {
                        _independantButton.gameObject.SetActive(false);
                        _independantText.gameObject.SetActive(false);

                        _getBackAtYourParentsButton.gameObject.SetActive(true);
                        _getBackAtYourParentsText.gameObject.SetActive(true);
                    }
                    else
                    {
                        _independantButton.gameObject.SetActive(true);
                        _independantText.gameObject.SetActive(true);
                        _independantButton.Button.Enabled = _gameManager.Money >= 1000;

                        _getBackAtYourParentsButton.gameObject.SetActive(false);
                        _getBackAtYourParentsText.gameObject.SetActive(false);
                    }
                }
                else
                {
                    _independantButton.gameObject.SetActive(false);
                    _independantText.gameObject.SetActive(false);
                    _getBackAtYourParentsButton.gameObject.SetActive(false);
                    _getBackAtYourParentsText.gameObject.SetActive(false);
                }
            }

            _lifeSlider.value = _gameManager.Life;
            _happinessSlider.value = _gameManager.Happiness;
            _moneyText.text = "$" + _gameManager.Money.ToString("#,0");

        }

        private void OnDestroy()
        {
            if (_gameManager != null)
            {
                _gameManager.OnLifeAction += GameManager_OnLifeAction;
            }
        }
    }
}
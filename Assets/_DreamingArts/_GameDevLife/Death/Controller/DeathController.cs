﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace DreamingArts.GameDevLife
{
    public class DeathController : MonoBehaviour
    {
        private int PARTY_DEATH_THRESHOLD = 2000;

        [SerializeField, BoxGroup("Death place")]
        string _movingOutDeath;

        [SerializeField, BoxGroup("Death causes")]
        string _depressionDeathCause;

        [SerializeField, BoxGroup("Death causes")]
        string _drugsDeathCause;

        [SerializeField, BoxGroup("Death causes")]
        string _naturalDeathCause;

        [SerializeField, BoxGroup("Happiness")]
        string _normalLife;

        [SerializeField, BoxGroup("Happiness")]
        string _happyLife;

        [SerializeField, BoxGroup("Happiness")]
        string _sadLife;

        // [SerializeField, BoxGroup("Money")]
        // string _normalLifeStyle;

        [SerializeField, BoxGroup("Money")]
        string _richLifeStyle;

        [SerializeField, BoxGroup("Money")]
        string _poorLifeStyle;

        [SerializeField, BoxGroup("Money")]
        string _richLifeStyleThenPoorDeath;

        [SerializeField, BoxGroup("Money")]
        Fun[] _partyFuns;

        [SerializeField, BoxGroup("Dreams")]
        string _workAsGameDeveloperDream;

        [SerializeField, BoxGroup("Dreams")]
        string _fundAcompanyDream;

        [SerializeField, BoxGroup("Dreams")]
        string _noDream;

        [SerializeField, BoxGroup("Dreams")]
        Job[] _workAsGameDeveloperJobs;

        [SerializeField, BoxGroup("Dreams")]
        Job _fundAcompanyJob;

        GameManager _context;
        private int _partyLifeCost;
        private int _maxMoney;
        private bool _workAsGameDeveloper;
        private bool _fundACompany;

        public GameManager Context { get => _context; }

        public void Setup(GameManager context)
        {
            _context = context;
        }

        public void Init()
        {
            _context.OnLifeAction += GameManager_OnLifeAction;
        }

        private void GameManager_OnLifeAction()
        {
            if (_context.Money > _maxMoney)
            {
                _maxMoney = _context.Money;
            }
            if (_context.LastLifeAction is FunController) {
                Fun fun = ((FunController)_context.LastLifeAction).Fun;
                for (int i = 0; i < _partyFuns.Length; ++i)
                {
                    if (fun == _partyFuns[i])
                    {
                        _partyLifeCost += fun.LifeCost;
                        break;
                    }
                }
            }
            if (!_workAsGameDeveloper)
            {
                if (_context.LastLifeAction is JobController)
                {
                    Job job = ((JobController)_context.LastLifeAction).Job;
                    for (int i = 0; i < _workAsGameDeveloperJobs.Length; ++i)
                    {
                        if (job == _workAsGameDeveloperJobs[i])
                        {
                            _workAsGameDeveloper = true;
                            break;
                        }
                    }
                }
            }
            else if (!_fundACompany)
            {
                if (_context.LastLifeAction is JobController)
                {
                    if (((JobController)_context.LastLifeAction).Job == _fundAcompanyJob)
                    {
                        _fundACompany = true;
                    }
                }
            }
        }

        public string GenerateText()
        {
            string deathPlace = null;
            if (_context.LastLifeAction == null)
            {
                deathPlace = _movingOutDeath;
            }
            else if (_context.LastLifeAction is FunController)
            {
                deathPlace = ((FunController)_context.LastLifeAction).Fun.DeathPlace;
            }
            else if (_context.LastLifeAction is LearningController)
            {
                deathPlace = ((LearningController)_context.LastLifeAction).Learning.DeathPlace;
            }
            else if (_context.LastLifeAction is JobController)
            {
                deathPlace = ((JobController)_context.LastLifeAction).Job.DeathPlace;
            }

            StringBuilder deathSentence = new StringBuilder();
            deathSentence.Append("<size=40><color=#ff0000>Your live is over</color></size>");
            deathSentence.AppendLine();
            deathSentence.AppendFormat(
                    "You've been found dead <nobr><color=#FFB3E9>{0}</color></nobr> <nobr>at the age of <color=#FFB3E9>{1} years old</color>.</nobr>",
                    deathPlace, _context.Age);

            deathSentence.AppendLine();

            if (_context.Money < 0)
            {
                if (_maxMoney > 100000)
                {
                    deathSentence.AppendLine(_richLifeStyleThenPoorDeath);
                }
                else
                {
                    deathSentence.AppendLine(_poorLifeStyle);
                }
            }
            else if (_context.Money > 100000)
            {
                deathSentence.AppendLine(_richLifeStyle);
            }
            //else
            //{
            //    deathSentence.AppendLine(_normalLifeStyle);
            //}

            bool naturalDeath = true;
            if (_context.Happiness < 0)
            {
                deathSentence.AppendLine(_depressionDeathCause);
                naturalDeath = false;
            }
            if (_partyLifeCost >= PARTY_DEATH_THRESHOLD)
            {
                deathSentence.AppendLine(_drugsDeathCause);
                naturalDeath = false;
            }
            if (naturalDeath)
            {
                deathSentence.AppendLine(_naturalDeathCause);
            }

            if (_fundACompany)
            {
                deathSentence.AppendLine(_fundAcompanyDream);

            }
            else if (_workAsGameDeveloper)
            {
                deathSentence.AppendLine(_workAsGameDeveloperDream);
            }
            else
            {
                deathSentence.AppendLine(_noDream);
            }

            float happinessPercent = _context.Happiness / _context.MaxHappiness;
            if (happinessPercent < 0.25f)
            {
                deathSentence.AppendLine(_sadLife);
            }
            else if (happinessPercent > 0.75f)
            {
                deathSentence.AppendLine(_happyLife);
            }
            else
            {
                deathSentence.AppendLine(_normalLife);
            }

            deathSentence.AppendLine("Rest in peace.");
            return deathSentence.ToString();
        }
    }
}
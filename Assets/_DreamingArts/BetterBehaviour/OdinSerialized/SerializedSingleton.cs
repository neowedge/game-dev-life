using UnityEngine;
using System.Collections;

namespace DreamingArts
{
    /// <author>Miguel López Ramírez</author>
    /// <summary>
    /// Singleton pattern
    /// </summary>
    /// <example>
    /// <code>
    /// public class MySingletonClass : com.KoronStudios.Singleton<MySingletonClass>
    /// {
    /// }
    ///	</code>
    /// </example>
    public abstract class SerializedSingleton<T> : SerializedBetterBehaviour where T : SerializedBetterBehaviour
    {
        private static bool _destroyed = false;

        private static T _instance = null;

        /// <summary>
        /// Get Singleton Instance
        /// </summary>
        public static T Instance
        {
            get
            {
                CreateInstance();

                return _instance;
            }
        }

        /// <summary>
        /// False if instance does not exists or has been destroyed
        /// </summary>
        public static bool HasInstance
        {
            get { return _instance != null; }
        }

        private static void CreateInstance()
        {
            if (!_destroyed && _instance == null)
            {
                _instance = FindObjectOfType<T>();

                if (_instance == null)
                {
                    GameObject go = new GameObject(typeof(T).Name);
                    _instance = go.AddComponent<T>();
                }
            }
        }

        /// <summary>
        /// Override Awake if you use it in children classes and call to base.Awake();
        /// </summary>
        virtual protected void Awake() {
            CreateInstance();
        }

        private void OnApplicationQuit()
        {
            _instance = null;
            _destroyed = true;
        }
    }
}
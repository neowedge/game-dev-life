﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamingArts.GameDevLife
{
    public class GameManager : MonoBehaviour
    {
        const int INDEPENDANT_MONTHS_OLD = 216; // 18 years old
        const int POCKET_MONEY_MONTHS_OLD = 180; // 15 years old

        [SerializeField]
        private int _startLife;

        [SerializeField]
        private int _maxHappiness;

        [SerializeField]
        private FunsManager _funsManager;

        [SerializeField]
        private LearningsManager _learningsManager;

        [SerializeField]
        private JobsManager _jobsManager;

        [SerializeField]
        private CharacterManager _characterManager;

        [SerializeField]
        private DeathController _deathController;

        [ShowInInspector, HideInEditorMode]
        private int _months;

        [ShowInInspector, HideInEditorMode]
        private int _life;

        [ShowInInspector, HideInEditorMode]
        private int _happiness;

        [ShowInInspector, HideInEditorMode]
        private int _money;

        private bool _independent = false;
        private LifeActionController _lastLifeAction;

        public int Months { get => _months; }
        public int Age { get => Mathf.FloorToInt(_months / 12); }
        public bool IndependentAge { get => _months >= INDEPENDANT_MONTHS_OLD; }
        public int Life { get => _life; }
        public int Happiness { get => _happiness; }
        public int Money { get => _money; }
        public bool Independent { get => _independent; }
        public LifeActionController LastLifeAction { get => _lastLifeAction; }

        public FunsManager FunsManager { get => _funsManager; }
        public LearningsManager LearningsManager { get => _learningsManager; }
        public JobsManager JobsManager { get => _jobsManager; }
        public CharacterManager CharacterManager { get => _characterManager; }
        public DeathController DeathController { get => _deathController; }

        public int MaxHappiness { get => _maxHappiness; }
        public int StartLife { get => _startLife; }

        public event System.Action OnLifeAction;
        public event System.Action OnGameEnd;

        public void Start()
        {
            _life = _startLife;
            _funsManager.Setup(this);
            _learningsManager.Setup(this);
            _jobsManager.Setup(this);
            _deathController.Setup(this);
        }

        public void StartGame()
        {
            _funsManager.Init();
            _learningsManager.Init();
            _jobsManager.Init();
            _deathController.Init();
        }

        public void BecameIndependent()
        {
            _independent = true;
            MakeLifeAction(null, -10, 100, 0);
        }

        public void GetBackAtYourParents()
        {
            _independent = false;
            MakeLifeAction(null, -10, 100, 0);
        }

        public void MakeLifeAction(LifeActionController lifeAction, int lifeChange, int happinessChange, int moneyChange)
        {
            _lastLifeAction = lifeAction;
            _life += lifeChange;
            _happiness += happinessChange;
            _money += moneyChange;

            if (IndependentAge)
            {
                if (_independent)
                {
                    _money -= 1000;
                }
                else
                {
                    _happiness -= (_months - INDEPENDANT_MONTHS_OLD) * 10;
                }
            }

            if (!_independent && _months >= POCKET_MONEY_MONTHS_OLD && moneyChange <= 0)
            {
                _money += 30; // Your parentes loves you ^^
            }

            if (_money < 0) {
                _happiness += _money;
            }
            if (_happiness < 0)
            {
                _life += _happiness;
            }

            ++_months;

            OnLifeAction?.Invoke();

            if (_life < 0)
            {
                EndGame();
            }
        }

        private void EndGame()
        {
            OnGameEnd?.Invoke();
        }

        public static string FormatLifeCost(int lifeCost)
        {
            if (lifeCost < 8)
            {
                return "Very low";
            }
            else if (lifeCost < 10)
            {
                return "Low"; 
            }
            else if (lifeCost > 15)
            {
                return "Too much";
            }
            else if (lifeCost > 12)
            {
                return "Very high";
            }
            else if (lifeCost > 10)
            {
                return "High";
            }
            else
            {
                return "Normal";
            }
        }
    }
}
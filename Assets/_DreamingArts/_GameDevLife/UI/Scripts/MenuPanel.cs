﻿using DG.Tweening;
using DreamingArts;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace DreamingArts.GameDevLife
{
    public class MenuPanel : MonoBehaviour
    {
        [SerializeField]
        private GameManager _gameManager;

        [SerializeField]
        private RectTransform _panel;

        [SerializeField]
        private ButtonDescription _funButton;

        [SerializeField]
        private ButtonDescription _learningButton;

        [SerializeField]
        private ButtonDescription _jobButton;

        [SerializeField]
        private RectTransform _funsContainer;

        [SerializeField]
        private RectTransform _learningsContainer;

        [SerializeField]
        private RectTransform _jobsContainer;

        [SerializeField]
        private JobActionButton _jobActionButtonPrefab;

        [SerializeField]
        private LearningActionButton _leaningActionButtonPrefab;

        [SerializeField]
        private FunActionButton _funActionButtonPrefab;

        [SerializeField]
        private TMP_Text _descriptionText;

        [SerializeField]
        private RectTransform _completeDescriptionPanel;

        [SerializeField]
        private TMP_Text _completeDescriptionText;

        [SerializeField, Multiline(5)]
        private string _funCompleteDescriptionTemplate;

        [SerializeField, Multiline(5)]
        private string _learningCompleteDescriptionTemplate;

        [SerializeField, Multiline(5)]
        private string _jobCompleteDescriptionTemplate;

        string _currentText;

        public string FunCompleteDescriptionTemplate { get => _funCompleteDescriptionTemplate; }
        public string LearningCompleteDescriptionTemplate { get => _learningCompleteDescriptionTemplate; }
        public string JobCompleteDescriptionTemplate { get => _jobCompleteDescriptionTemplate; }

        public Tween Show()
        {
            _panel.anchoredPosition = new Vector2(0, -222f);
            _panel.gameObject.SetActive(true);

            _funButton.Setup(this);
            _learningButton.Setup(this);
            _jobButton.Setup(this);

            return _panel.DOAnchorPosY(0, 0.2f)
                    .OnComplete(Init);
        }

        public void Init()
        {
            using (IEnumerator<FunController> e = _gameManager.FunsManager.GetEnumerator())
            {
                while (e.MoveNext())
                {
                    Instantiate<FunActionButton>(_funActionButtonPrefab, _funsContainer)
                            .Setup(e.Current, this);
                }
            }

            using (IEnumerator<LearningController> e = _gameManager.LearningsManager.GetEnumerator())
            {
                while (e.MoveNext())
                {
                    Instantiate<LearningActionButton>(_leaningActionButtonPrefab, _learningsContainer)
                            .Setup(e.Current, this);
                }
            }

            for (int i = 0; i < _gameManager.JobsManager.JobsSlots.Length; ++i)
            {
                Instantiate<JobActionButton>(_jobActionButtonPrefab, _jobsContainer)
                            .Setup(_gameManager.JobsManager.JobsSlots[i], this);
            }

            SelectFun();
        }

        public void SelectFun()
        {
            _funButton.Button.Enabled = false;
            _learningButton.Button.Enabled = true;
            _jobButton.Button.Enabled = true;

            _funsContainer.gameObject.SetActive(true);
            _learningsContainer.gameObject.SetActive(false);
            _jobsContainer.gameObject.SetActive(false);

            _currentText = _funButton.Description;
            _descriptionText.text = _currentText;
        }

        public void SelectLearning()
        {
            _funButton.Button.Enabled = true;
            _learningButton.Button.Enabled = false;
            _jobButton.Button.Enabled = true;

            _funsContainer.gameObject.SetActive(false);
            _learningsContainer.gameObject.SetActive(true);
            _jobsContainer.gameObject.SetActive(false);

            _currentText = _learningButton.Description;
            _descriptionText.text = _currentText;
        }

        public void SelectJob()
        {
            _funButton.Button.Enabled = true;
            _learningButton.Button.Enabled = true;
            _jobButton.Button.Enabled = false;

            _funsContainer.gameObject.SetActive(false);
            _learningsContainer.gameObject.SetActive(false);
            _jobsContainer.gameObject.SetActive(true);

            _currentText = _jobButton.Description;
            _descriptionText.text = _currentText;
        }

        public void SetDescriptionText(string text)
        {
            _descriptionText.text = text;
        }

        public void UnsetDescriptionText()
        {
            _descriptionText.text = _currentText;
        }

        public void SetCompleteDescriptionText(string text)
        {
            _completeDescriptionText.text = text;
            _completeDescriptionPanel.gameObject.SetActive(true);
        }

        public void UnsetCompleteDescriptionText()
        {
            _completeDescriptionText.text = null;
            _completeDescriptionPanel.gameObject.SetActive(false);
        }
    }
}
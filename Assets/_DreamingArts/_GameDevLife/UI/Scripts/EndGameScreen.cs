﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DreamingArts.GameDevLife
{
    public class EndGameScreen : MonoBehaviour
    {
        [SerializeField]
        GameManager _gameManager;

        [SerializeField]
        CanvasGroup _deathCanvasGroup;

        [SerializeField]
        TMP_Text _deathText;

        [SerializeField]
        Button _retryButton;

        private void OnDestroy()
        {
            if (_gameManager != null)
            {
                _gameManager.OnGameEnd -= GameManager_OnGameEnd;
            }
        }

        void Start()
        {
            _gameManager.OnGameEnd += GameManager_OnGameEnd;
        }

        private void GameManager_OnGameEnd()
        {
            _deathCanvasGroup.alpha = 0;
            _deathCanvasGroup.gameObject.SetActive(true);

            _deathText.text = _gameManager.DeathController.GenerateText();
            _retryButton.Enabled = false;

            DOTween.Sequence()
                .Append(_deathCanvasGroup.DOFade(1f, 2f))
                .AppendInterval(5f)
                .OnComplete(() =>
                {
                    _retryButton.Enabled = true;
                });
        }

        public void RetryGame()
        {
            _retryButton.Enabled = false;
            SceneManager.LoadScene(0);
        }
    }
}
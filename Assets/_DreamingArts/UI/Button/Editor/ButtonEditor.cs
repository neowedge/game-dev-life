/*
 * ButtonEditor
 * Copyright (c) Koron Studios
 * http://www.koronstudios.com/
 *
 * Created on 21/2/2018 14:25:05
 */

using UnityEngine;
using UnityEditor;

namespace com.DreamingArts.DeathTycoon {

    /// <summary>
    /// Describe your class
    /// </summary>
    public class ButtonEditor {
        [MenuItem("GameObject/UI/Better Button")]
        static void CreatePrefab() {
            GameObject go = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(
                    "Assets/_DreamingArts/UI/Button/Prefabs/Button.prefab")) as GameObject;

            Transform parent = null;
            if (Selection.activeObject != null) {
                parent = (Selection.activeObject as GameObject).GetComponent<RectTransform>();
            }

            if (parent == null) {
                Canvas canvas = GameObject.FindObjectOfType<Canvas>();
                if (canvas != null) {
                    parent = canvas.transform;
                } else {
                    GameObject parentGo = new GameObject("Canvas", new System.Type[] {
                            typeof(Canvas),
                            typeof(UnityEngine.UI.CanvasScaler),
                            typeof(UnityEngine.UI.GraphicRaycaster)
                    });
                    canvas = parentGo.GetComponent<Canvas>();
                    canvas.renderMode = RenderMode.ScreenSpaceOverlay;


                    parent = parentGo.transform;

                    UnityEngine.EventSystems.EventSystem eventSystem = GameObject.FindObjectOfType<UnityEngine.EventSystems.EventSystem>();

                    if (eventSystem == null) {
                        new GameObject("EventSystem", new System.Type[] {
                                typeof(UnityEngine.EventSystems.EventSystem),
                                typeof(UnityEngine.EventSystems.StandaloneInputModule),
                        });
                    }
                }
            }

            go.transform.SetParent(parent, false);
            //PrefabUtility.UnloadPrefabContents(go);
            PrefabUtility.UnpackPrefabInstance(go, PrefabUnpackMode.Completely, InteractionMode.AutomatedAction);
            Selection.activeObject = go;
        }
    }
}
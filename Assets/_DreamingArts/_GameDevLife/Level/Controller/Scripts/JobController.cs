﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamingArts.GameDevLife
{
    public class JobController : LifeActionController
    {
        private Job _job;
        private JobsSlot _slot;

        public Job Job { get => _job; }
        public JobsSlot Slot { get => _slot; }

        public static event System.Action<JobController> OnAnyLevelUp;

        public JobController(Job job, JobsSlot slot)
        {
            _job = job;
            _slot = slot;
        }

        public void Init()
        {
            Unlocked = IsUnlocked();
            if (!Unlocked)
            {
                LearningController.OnAnyLevelUp += Learning_OnAnyLevelUp;
                JobController.OnAnyLevelUp += Job_OnAnyLevelUp;
            }
        }

        public override void LevelUp()
        {
            base.LevelUp();
            _slot.Context.GameManager.MakeLifeAction(this, -_job.LifeCost, -_job.HappinessCost, _job.Salary);
            OnAnyLevelUp?.Invoke(this);
        }

        protected override bool IsUnlocked()
        {
            bool unlocked = true;
            foreach (KeyValuePair<Job, int> requirement in _job.JobRequirements)
            {
                if (_slot.Context.Get(requirement.Key).Level < requirement.Value)
                {
                    unlocked = false;
                    break;
                }
            }
            if (unlocked)
            {
                foreach (KeyValuePair<Learning, int> requirement in _job.LearningRequirements)
                {
                    if (_slot.Context.GameManager.LearningsManager.Get(requirement.Key).Level < requirement.Value)
                    {
                        unlocked = false;
                        break;
                    }
                }
            }

            return unlocked;
        }

        private void Learning_OnAnyLevelUp(LearningController learning)
        {
            if (IsUnlocked())
            {
                Unlocked = true;
                LearningController.OnAnyLevelUp -= Learning_OnAnyLevelUp;
                JobController.OnAnyLevelUp -= Job_OnAnyLevelUp;
            }
        }

        private void Job_OnAnyLevelUp(JobController job)
        {
            if (IsUnlocked())
            {
                Unlocked = true;
                LearningController.OnAnyLevelUp -= Learning_OnAnyLevelUp;
                JobController.OnAnyLevelUp -= Job_OnAnyLevelUp;
            }
        }

        public void Dispose()
        {
            LearningController.OnAnyLevelUp -= Learning_OnAnyLevelUp;
            JobController.OnAnyLevelUp -= Job_OnAnyLevelUp;
        }
    }
}
﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamingArts.GameDevLife
{
    public class LearningsManager : MonoBehaviour
    {
        [ShowInInspector, HideInEditorMode]
        private Dictionary<Learning, LearningController> _learningControllers;

        GameManager _gameManager;

        public GameManager GameManager { get => _gameManager; }

        public IEnumerator<LearningController> GetEnumerator()
        {
            return _learningControllers.Values.GetEnumerator();
        }

        public LearningController Get(Learning learning)
        {
            return _learningControllers[learning];
        }

        public void Setup(GameManager gameManager)
        {
            _gameManager = gameManager;

            Learning[] learningList = Resources.LoadAll<Learning>("Learning");
            _learningControllers = new Dictionary<Learning, LearningController>(learningList.Length);
            for (int i = 0; i < learningList.Length; ++i)
            {
                _learningControllers.Add(learningList[i], new LearningController(learningList[i], this));
            }
        }

        public void Init()
        {
            IEnumerator<LearningController> e = _learningControllers.Values.GetEnumerator();
            while (e.MoveNext())
            {
                e.Current.Init();
            }
        }


        private void OnDestroy()
        {
            if (_learningControllers != null)
            {
                IEnumerator<LearningController> e = _learningControllers.Values.GetEnumerator();
                while (e.MoveNext())
                {
                    e.Current.Dispose();
                }
            }
        }
    }
}
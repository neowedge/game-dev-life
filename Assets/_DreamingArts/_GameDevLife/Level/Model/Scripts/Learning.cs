﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamingArts.GameDevLife
{
    [CreateAssetMenu(menuName = "[Game Dev Life] Learning")]
    public class Learning : SerializedScriptableObject
    {
        [SerializeField]
        private string _name;

        [SerializeField]
        private string _description;

        [SerializeField]
        private string _deathPlace;

        [SerializeField]
        private int _ageRequired;

        [SerializeField]
        private Dictionary<Learning, int> _learningRequirements;

        [SerializeField]
        private int _lifeCost;

        [SerializeField]
        private int _happinessCost;

        public string Name { get => _name; }
        public string Description { get => _description; }
        public string DeathPlace { get => _deathPlace; }
        public int AgeRequired { get => _ageRequired; }
        public Dictionary<Learning, int> LearningRequirements { get => _learningRequirements; }
        public int LifeCost { get => _lifeCost; }
        public int HappinessCost { get => _happinessCost; }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DreamingArts.GameDevLife
{
    [RequireComponent(typeof(Button))]
    public class ButtonDescription : BetterBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField]
        protected string _description;

        protected Button _button;
        protected MenuPanel _menuPanel;

        public Button Button {
            get {
                if (_button == null)
                {
                    _button = GetComponent<Button>();
                }
                return _button;
            }
        }

        public string Description { get => _description; }

        public virtual void OnPointerEnter(PointerEventData eventData)
        {
            _menuPanel.SetDescriptionText(_description);
        }

        public virtual void OnPointerExit(PointerEventData eventData)
        {
            _menuPanel.UnsetDescriptionText();
        }

        public void Setup(MenuPanel menuPanel)
        {
            _menuPanel = menuPanel;
        }
    }
}

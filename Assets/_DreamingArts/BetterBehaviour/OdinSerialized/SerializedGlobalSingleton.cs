using UnityEngine;
using System.Collections;

namespace DreamingArts {
    /// <author>Miguel López Ramírez</author>
    /// <summary>
    /// Singleton pattern with global persistance between scenes
    /// </summary>
    /// <example>
    /// <code>
    /// public class MyGlobalSingletonClass : com.KoronStudios.GlobalSingleton<MyGlobalSingletonClass>
    /// {
    /// }
    ///	</code>
    /// </example>
    public abstract class SerializedGlobalSingleton<T> : SerializedBetterBehaviour where T : SerializedBetterBehaviour
    {
        static bool _destroyed = false;

        static T _instance = null;

        /// <summary>
        /// Get GlobalSingleton Instance
        /// </summary>
        public static T Instance
        {
            get
            {
                CreateInstance();

                return _instance;
            }
        }

        /// <summary>
        /// False if instance does not exists or has been destroyed
        /// </summary>
        public static bool HasInstance
        {
            
            get { return _instance != null; }
        }

        static void CreateInstance()
        {
            if (!_destroyed && _instance == null)
            {
                _instance = FindObjectOfType<T>();

                if (_instance == null)
                {
                    GameObject go = new GameObject(typeof(T).Name);
                    _instance = go.AddComponent<T>();
                }

                DontDestroyOnLoad(_instance.gameObject);
            }
        }

        /// <summary>
        /// Override Awake if you use it in children classes and call to base.Awake();
        /// </summary>
        virtual protected void Awake()
        {
            CreateInstance();
        }

        void OnApplicationQuit()
        {
            _instance = null;
            _destroyed = true;
        }
    }
}
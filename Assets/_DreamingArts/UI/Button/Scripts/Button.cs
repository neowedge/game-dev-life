﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;
using MEC;
using TMPro;

namespace DreamingArts {

    [ExecuteInEditMode]
    public class Button : BetterBehaviour, 
            IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler,
            IDragHandler, IBeginDragHandler, IEndDragHandler {

        #region Types

        public enum ButtonMode {
            OnClick,
            OnDown,
            OnDownAndRelease,
            OnHold,
        }

        #endregion

        #region Unity Inspector

        [SerializeField, BoxGroup("Button Behaviour")]
        private ButtonMode _mode;

        [SerializeField, BoxGroup("Button Behaviour"), ShowIf("_mode", ButtonMode.OnHold)]
        private float _firstActionDelay = 0.5f;

        [SerializeField, BoxGroup("Button Behaviour"), ShowIf("_mode", ButtonMode.OnHold)]
        private float _holdActionRate = 0.1f;

        [SerializeField, BoxGroup("Button Behaviour")]
        private bool _enabled = true;

#if UNITY_EDITOR
        [Button, BoxGroup("Button Behaviour"), HideIf("_enabled")]
        private void SetAsEnabled() {

            if (!_enabled)
            {
                _enabled = true;

                if (_resizeObject != null)
                {
                    _resizeObject.sizeDelta = _resizeObject.sizeDelta
                            - _stylePresets.Disabled.Resize + _stylePresets.Normal.Resize;
                }

                if (_rescaleObject != null)
                {
                    _rescaleObject.localScale = _rescaleObject.localScale - (Vector3)
                            (_stylePresets.Disabled.Rescale -
                            _stylePresets.Normal.Rescale);
                }
            }

            for (int i = 0; i < _bottomImages.Length; ++i)
            {
                _bottomImages[i].color = _stylePresets.Normal.Style.BottomColor;
            }
            for (int i = 0; i < _strokeImages.Length; ++i)
            {
                _strokeImages[i].color = _stylePresets.Normal.Style.StrokeColor;
            }
            for (int i = 0; i < _baseImages.Length; ++i)
            {
                _baseImages[i].color = _stylePresets.Normal.Style.BaseColor;
            }
            for (int i = 0; i < _baseTexts.Length; ++i)
            {
                _baseTexts[i].color = _stylePresets.Normal.Style.BaseTextColor;
            }
            for (int i = 0; i < _innerPanelImages.Length; ++i)
            {
                _innerPanelImages[i].color = _stylePresets.Normal.Style.InnerPanelColor;
            }
            for (int i = 0; i < _innerPanelTexts.Length; ++i)
            {
                _innerPanelTexts[i].color = _stylePresets.Normal.Style.InnerPanelTextColor;
            }

            for (int i = 0; i < _tintGraphics.Length; ++i)
            {
                _tintGraphics[i].CrossFadeColor(
                        _stylePresets.Normal.TintColor,
                        0,
                        true,
                        true);
            }

            UnityEditor.EditorUtility.SetDirty(this);
        }

        [Button, BoxGroup("Button Behaviour"), ShowIf("_enabled")]
        private void SetAsDisabled() {

            if (_enabled)
            {
                _enabled = false;

                if (_resizeObject != null)
                {
                    _resizeObject.sizeDelta = _resizeObject.sizeDelta -
                            _stylePresets.Normal.Resize + _stylePresets.Disabled.Resize;
                }

                if (_rescaleObject != null)
                {
                    _rescaleObject.localScale = _rescaleObject.localScale - (Vector3)
                            (_stylePresets.Normal.Rescale -
                            _stylePresets.Disabled.Rescale);
                }
            }

            for (int i = 0; i < _bottomImages.Length; ++i)
            {
                _bottomImages[i].color = _stylePresets.Disabled.Style.BottomColor;
            }
            for (int i = 0; i < _strokeImages.Length; ++i)
            {
                _strokeImages[i].color = _stylePresets.Disabled.Style.StrokeColor;
            }
            for (int i = 0; i < _baseImages.Length; ++i)
            {
                _baseImages[i].color = _stylePresets.Disabled.Style.BaseColor;
            }
            for (int i = 0; i < _baseTexts.Length; ++i)
            {
                _baseTexts[i].color = _stylePresets.Disabled.Style.BaseTextColor;
            }
            for (int i = 0; i < _innerPanelImages.Length; ++i)
            {
                _innerPanelImages[i].color = _stylePresets.Disabled.Style.InnerPanelColor;
            }
            for (int i = 0; i < _innerPanelTexts.Length; ++i)
            {
                _innerPanelTexts[i].color = _stylePresets.Disabled.Style.InnerPanelTextColor;
            }

            for (int i = 0; i < _tintGraphics.Length; ++i)
            {
                _tintGraphics[i].CrossFadeColor(
                        _stylePresets.Disabled.TintColor,
                        0,
                        true,
                        true);
            }
        }

        [Button, BoxGroup("Button Behaviour"), ShowIf("CanPress")]
        private void SetAsPressed()
        {
            if (!_isPressed)
            {
                _isPressed = true;

                if (_resizeObject != null)
                {
                    _resizeObject.sizeDelta = _resizeObject.sizeDelta -
                            _stylePresets.Normal.Resize + _stylePresets.Pressed.Resize;
                }

                if (_rescaleObject != null)
                {
                    _rescaleObject.localScale = _rescaleObject.localScale - (Vector3)
                            (_stylePresets.Normal.Rescale -
                            _stylePresets.Pressed.Rescale);
                }
            }

            for (int i = 0; i < _bottomImages.Length; ++i)
            {
                _bottomImages[i].color = _stylePresets.Pressed.Style.BottomColor;
            }
            for (int i = 0; i < _strokeImages.Length; ++i)
            {
                _strokeImages[i].color = _stylePresets.Pressed.Style.StrokeColor;
            }
            for (int i = 0; i < _baseImages.Length; ++i)
            {
                _baseImages[i].color = _stylePresets.Pressed.Style.BaseColor;
            }
            for (int i = 0; i < _baseTexts.Length; ++i)
            {
                _baseTexts[i].color = _stylePresets.Pressed.Style.BaseTextColor;
            }
            for (int i = 0; i < _innerPanelImages.Length; ++i)
            {
                _innerPanelImages[i].color = _stylePresets.Pressed.Style.InnerPanelColor;
            }
            for (int i = 0; i < _innerPanelTexts.Length; ++i)
            {
                _innerPanelTexts[i].color = _stylePresets.Pressed.Style.InnerPanelTextColor;
            }

            for (int i = 0; i < _tintGraphics.Length; ++i)
            {
                _tintGraphics[i].CrossFadeColor(
                        _stylePresets.Pressed.TintColor,
                        0,
                        true,
                        true);
            }

            UnityEditor.EditorUtility.SetDirty(this);
        }

        [Button, BoxGroup("Button Behaviour"), ShowIf("CanUnPress")]
        private void SetAsNotPressed()
        {
            if (_isPressed)
            {
                _isPressed = false;

                if (_resizeObject != null)
                {
                    _resizeObject.sizeDelta = _resizeObject.sizeDelta -
                            _stylePresets.Pressed.Resize + _stylePresets.Normal.Resize;
                }

                if (_rescaleObject != null)
                {
                    _rescaleObject.localScale = _rescaleObject.localScale - (Vector3)
                            (_stylePresets.Pressed.Rescale -
                            _stylePresets.Normal.Rescale);
                }
            }

            for (int i = 0; i < _bottomImages.Length; ++i)
            {
                _bottomImages[i].color = _stylePresets.Normal.Style.BottomColor;
            }
            for (int i = 0; i < _strokeImages.Length; ++i)
            {
                _strokeImages[i].color = _stylePresets.Normal.Style.StrokeColor;
            }
            for (int i = 0; i < _baseImages.Length; ++i)
            {
                _baseImages[i].color = _stylePresets.Normal.Style.BaseColor;
            }
            for (int i = 0; i < _baseTexts.Length; ++i)
            {
                _baseTexts[i].color = _stylePresets.Normal.Style.BaseTextColor;
            }
            for (int i = 0; i < _innerPanelImages.Length; ++i)
            {
                _innerPanelImages[i].color = _stylePresets.Normal.Style.InnerPanelColor;
            }
            for (int i = 0; i < _innerPanelTexts.Length; ++i)
            {
                _innerPanelTexts[i].color = _stylePresets.Normal.Style.InnerPanelTextColor;
            }

            for (int i = 0; i < _tintGraphics.Length; ++i)
            {
                _tintGraphics[i].CrossFadeColor(
                        _stylePresets.Normal.TintColor,
                        0,
                        true,
                        true);
            }

            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif

        [SerializeField, DrawWithUnity, BoxGroup("Button Behaviour")]
        private UnityEvent _action;

        [SerializeField, DrawWithUnity, BoxGroup("Button Behaviour"), ShowIf("_mode", ButtonMode.OnDownAndRelease)]
        private UnityEvent _releaseAction;

        [SerializeField, DrawWithUnity, BoxGroup("Button Behaviour")]
        private UnityEvent _disabledAction;

        [SerializeField, BoxGroup("Button Style")]
        private ButtonStylePresets _stylePresets;

        [SerializeField, BoxGroup("Button Style")]
        private Image[] _bottomImages;

        [SerializeField, BoxGroup("Button Style")]
        private Image[] _strokeImages;

        [SerializeField, BoxGroup("Button Style")]
        private Image[] _baseImages;

        [SerializeField, BoxGroup("Button Style")]
        private TMP_Text[] _baseTexts;

        [SerializeField, BoxGroup("Button Style")]
        private Image[] _innerPanelImages;

        [SerializeField, BoxGroup("Button Style")]
        private TMP_Text[] _innerPanelTexts;

        [SerializeField, BoxGroup("Button Style")]
        private Graphic[] _tintGraphics;

        [SerializeField, BoxGroup("Button Style")]
        private RectTransform _resizeObject;

        [SerializeField, BoxGroup("Button Style")]
        private RectTransform _rescaleObject;

        [SerializeField, DisableInEditorMode]
        private Vector2 _normalSize;

        [SerializeField, DisableInEditorMode]
        private Vector2 _pressedSize;

        [SerializeField, DisableInEditorMode]
        private Vector2 _disabledSize;

        [SerializeField, DisableInEditorMode]
        private Vector3 _normalScale;

        [SerializeField, DisableInEditorMode]
        private Vector3 _pressedScale;

        [SerializeField, DisableInEditorMode]
        private Vector3 _disabledScale;

#if UNITY_EDITOR
        [Button, BoxGroup("Button Style")]
        private void SetAllChildrenAsTintGraphics() {
            _tintGraphics = GetComponentsInChildren<Graphic>(true);
        }

        [Button, BoxGroup("Button Style"), ShowIf("HasTintGraphics")]
        private void DeleteAllTintGraphics() {
            _tintGraphics = new Graphic[0];
        }

        [Button, BoxGroup("Style Helpers")]
        private void SaveCurrentAsNormalColors()
        {
            _stylePresets.Normal.Style.BottomColor = 
                    _bottomImages.Length > 0 ? _bottomImages[0].color : Color.white;
            _stylePresets.Normal.Style.StrokeColor = 
                    _strokeImages.Length > 0 ? _strokeImages[0].color : Color.white;
            _stylePresets.Normal.Style.BaseColor =
                    _baseImages.Length > 0 ? _baseImages[0].color : Color.white;
            _stylePresets.Normal.Style.BaseTextColor =
                    _baseTexts.Length > 0 ? _baseTexts[0].color : Color.white;
            _stylePresets.Normal.Style.InnerPanelColor =
                    _innerPanelImages.Length > 0 ? _innerPanelImages[0].color : Color.white;
            _stylePresets.Normal.Style.InnerPanelTextColor =
                    _innerPanelTexts.Length > 0 ? _innerPanelTexts[0].color : Color.white;

            UnityEditor.EditorUtility.SetDirty(_stylePresets.Normal.Style);
            UnityEditor.AssetDatabase.SaveAssets();
        }

        [Button, BoxGroup("Style Helpers")]
        private void SaveCurrentAsPressedColors()
        {
            _stylePresets.Pressed.Style.BottomColor =
                    _bottomImages.Length > 0 ? _bottomImages[0].color : Color.white;
            _stylePresets.Pressed.Style.StrokeColor =
                    _strokeImages.Length > 0 ? _strokeImages[0].color : Color.white;
            _stylePresets.Pressed.Style.BaseColor =
                    _baseImages.Length > 0 ? _baseImages[0].color : Color.white;
            _stylePresets.Pressed.Style.BaseTextColor =
                    _baseTexts.Length > 0 ? _baseTexts[0].color : Color.white;
            _stylePresets.Pressed.Style.InnerPanelColor =
                    _innerPanelImages.Length > 0 ? _innerPanelImages[0].color : Color.white;
            _stylePresets.Pressed.Style.InnerPanelTextColor =
                    _innerPanelTexts.Length > 0 ? _innerPanelTexts[0].color : Color.white;

            UnityEditor.EditorUtility.SetDirty(_stylePresets.Pressed.Style);
            UnityEditor.AssetDatabase.SaveAssets();
        }

        [Button, BoxGroup("Style Helpers")]
        private void SaveCurrentAsDisabledColors()
        {
            _stylePresets.Disabled.Style.BottomColor =
                    _bottomImages.Length > 0 ? _bottomImages[0].color : Color.white;
            _stylePresets.Disabled.Style.StrokeColor =
                    _strokeImages.Length > 0 ? _strokeImages[0].color : Color.white;
            _stylePresets.Disabled.Style.BaseColor =
                    _baseImages.Length > 0 ? _baseImages[0].color : Color.white;
            _stylePresets.Disabled.Style.BaseTextColor =
                    _baseTexts.Length > 0 ? _baseTexts[0].color : Color.white;
            _stylePresets.Disabled.Style.InnerPanelColor =
                    _innerPanelImages.Length > 0 ? _innerPanelImages[0].color : Color.white;
            _stylePresets.Disabled.Style.InnerPanelTextColor =
                    _innerPanelTexts.Length > 0 ? _innerPanelTexts[0].color : Color.white;

            UnityEditor.EditorUtility.SetDirty(_stylePresets.Disabled.Style);
            UnityEditor.AssetDatabase.SaveAssets();
        }

        private void OnRectTransformDimensionsChange()
        {
            if (!Application.isPlaying)
            {
                Setup();
            }
        }

        [Button]
        private void Setup()
        {
            if (_enabled)
            {
                if (_resizeObject != null)
                {
                    _normalSize = _resizeObject.sizeDelta;
                    _pressedSize = _resizeObject.sizeDelta -
                            _stylePresets.Normal.Resize + _stylePresets.Pressed.Resize;
                    _disabledSize = _resizeObject.sizeDelta -
                            _stylePresets.Normal.Resize + _stylePresets.Disabled.Resize;
                }

                if (_rescaleObject != null)
                {
                    _normalScale = _rescaleObject.localScale;
                    _pressedScale = _rescaleObject.localScale - (Vector3)
                            (_stylePresets.Normal.Rescale -
                            _stylePresets.Pressed.Rescale);
                    _disabledScale = _rescaleObject.localScale - (Vector3)
                            (_stylePresets.Normal.Rescale -
                            _stylePresets.Disabled.Rescale);
                }
            }
            else if (_isPressed)
            {
                if (_resizeObject != null)
                {
                    _normalSize = _resizeObject.sizeDelta -
                        _stylePresets.Pressed.Resize + _stylePresets.Normal.Resize;
                    _pressedSize = _resizeObject.sizeDelta;
                    _disabledSize = _resizeObject.sizeDelta -
                        _stylePresets.Pressed.Resize + _stylePresets.Disabled.Resize;
                }

                if (_rescaleObject != null)
                {
                    _normalScale = _rescaleObject.localScale - (Vector3)
                        (_stylePresets.Pressed.Rescale -
                        _stylePresets.Normal.Rescale);
                    _pressedScale = _rescaleObject.localScale;
                    _disabledScale = _rescaleObject.localScale - (Vector3)
                        (_stylePresets.Pressed.Rescale -
                        _stylePresets.Disabled.Rescale);
                }
            }
            else
            {
                if (_resizeObject != null)
                {
                    _normalSize = _resizeObject.sizeDelta -
                        _stylePresets.Disabled.Resize + _stylePresets.Normal.Resize;
                    _pressedSize = _resizeObject.sizeDelta -
                            _stylePresets.Disabled.Resize + _stylePresets.Pressed.Resize;
                    _disabledSize = _resizeObject.sizeDelta;
                }

                if (_rescaleObject != null)
                {
                    _normalScale = _rescaleObject.localScale - (Vector3)
                        (_stylePresets.Disabled.Rescale -
                        _stylePresets.Normal.Rescale);
                    _pressedScale = _rescaleObject.localScale - (Vector3)
                            (_stylePresets.Disabled.Rescale -
                            _stylePresets.Pressed.Rescale);
                    _disabledScale = _rescaleObject.localScale;
                }
            }

            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif

#if UNITY_EDITOR

        private bool HasTintGraphics() {
            return _tintGraphics.Length > 0;
        }

        private bool HasChildren() {
            return transform.childCount > 0;
        }

        private bool HasChildrenImage() {
            return HasChildren() && transform.GetChild(0).GetComponentInChildren<Image>() != null;
        }

        private bool _isPressed;
        private bool CanPress()
        {
            return _enabled && !_isPressed;
        }

        private bool CanUnPress()
        {
            return _enabled && _isPressed;
        }
#endif

        #endregion

        #region Private Fields
        private bool _initialized;
        private ScrollRect _parentScrollRect = null;

        private Vector2 _translateOriginPosition;
        private bool _pointerDown;
        private bool _pointerHover;
        private bool _repeating;

        private bool _dragging;
        #endregion

        #region Public Properties

        public bool Enabled {
            get {
                return _enabled;
            }

            set {
                if (value != _enabled) {
                    _enabled = value;
                    if (_enabled) {
                        PlayEnableAnimation();
                    } else {
                        if (_pointerDown) {
                            if (_mode == ButtonMode.OnDownAndRelease)
                            {
                                _releaseAction.Invoke();
                            }
                            PlayReleaseAnimation();
                            _pointerDown = false;
                            
                        } else {
                            PlayDisableAnimation();
                        }
                        _pointerHover = false;
                    }
                }
            }
        }

        public UnityEvent Action {
            get {
                return _action;
            }
        }

        public UnityEvent ReleaseAction {
            get {
                return _releaseAction;
            }
        }

        public UnityEvent NoInteractableAction {
            get {
                return _disabledAction;
            }
        }

        #endregion

        #region MonoBehaviour
        private void Awake()
        {
            _parentScrollRect = GetComponentInParent<ScrollRect>();
        }

        private void OnEnable() {
            //Setup();
            if (_enabled) {
                if (_resizeObject != null)
                {
                    _resizeObject.sizeDelta = _normalSize;
                }
                if (_rescaleObject != null)
                {
                    _rescaleObject.localScale = _normalScale;
                }

                for (int i = 0; i < _bottomImages.Length; ++i)
                {
                    _bottomImages[i].color = _stylePresets.Normal.Style.BottomColor;
                }
                for (int i = 0; i < _strokeImages.Length; ++i)
                {
                    _strokeImages[i].color = _stylePresets.Normal.Style.StrokeColor;
                }
                for (int i = 0; i < _baseImages.Length; ++i)
                {
                    _baseImages[i].color = _stylePresets.Normal.Style.BaseColor;
                }
                for (int i = 0; i < _baseTexts.Length; ++i)
                {
                    _baseTexts[i].color = _stylePresets.Normal.Style.BaseTextColor;
                }
                for (int i = 0; i < _innerPanelImages.Length; ++i)
                {
                    _innerPanelImages[i].color = _stylePresets.Normal.Style.InnerPanelColor;
                }
                for (int i = 0; i < _innerPanelTexts.Length; ++i)
                {
                    _innerPanelTexts[i].color = _stylePresets.Normal.Style.InnerPanelTextColor;
                }

                for (int i = 0; i < _tintGraphics.Length; ++i)
                {
                    _tintGraphics[i].CrossFadeColor(
                            _stylePresets.Normal.TintColor,
                            0,
                            true,
                            true);
                }
            } else {
                if (_resizeObject != null)
                {
                    _resizeObject.sizeDelta = _disabledSize;
                }
                if (_rescaleObject != null)
                {
                    _rescaleObject.localScale = _disabledScale;
                }

                for (int i = 0; i < _bottomImages.Length; ++i)
                {
                    _bottomImages[i].color = _stylePresets.Disabled.Style.BottomColor;
                }
                for (int i = 0; i < _strokeImages.Length; ++i)
                {
                    _strokeImages[i].color = _stylePresets.Disabled.Style.StrokeColor;
                }
                for (int i = 0; i < _baseImages.Length; ++i)
                {
                    _baseImages[i].color = _stylePresets.Disabled.Style.BaseColor;
                }
                for (int i = 0; i < _baseTexts.Length; ++i)
                {
                    _baseTexts[i].color = _stylePresets.Disabled.Style.BaseTextColor;
                }
                for (int i = 0; i < _innerPanelImages.Length; ++i)
                {
                    _innerPanelImages[i].color = _stylePresets.Disabled.Style.InnerPanelColor;
                }
                for (int i = 0; i < _innerPanelTexts.Length; ++i)
                {
                    _innerPanelTexts[i].color = _stylePresets.Disabled.Style.InnerPanelTextColor;
                }

                for (int i = 0; i < _tintGraphics.Length; ++i)
                {
                    _tintGraphics[i].CrossFadeColor(
                            _stylePresets.Normal.TintColor,
                            0,
                            true,
                            true);
                }
            }
        }

        private void OnDisable() {
            if (_pointerDown)
            {
                if (_mode == ButtonMode.OnDownAndRelease)
                {
                    _releaseAction.Invoke();
                }
                _pointerDown = false;
            }
            _pointerHover = false;
        }

        #endregion

        #region Pointer Events Handlers

        public void OnPointerDown(PointerEventData eventData) {
            if (_enabled) {
                PlayPressAnimation();

                if (_mode == ButtonMode.OnDown || _mode == ButtonMode.OnDownAndRelease) {
                    _action.Invoke();
                } else if (_mode == ButtonMode.OnHold) {
                    Timing.CallDelayed(_firstActionDelay, StartHold, gameObject);
                }

                _pointerDown = true;
                _pointerHover = true;
            } else {
                _disabledAction.Invoke();
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (_enabled && _pointerDown)
            {
                if (_mode == ButtonMode.OnDownAndRelease)
                {
                    _releaseAction.Invoke();
                }
                else if (_pointerHover)
                {
                    if (_mode == ButtonMode.OnClick || _mode == ButtonMode.OnHold)
                    {
                        _action.Invoke();
                    }
                }
                PlayReleaseAnimation();
            }

            _pointerHover = false;
            _pointerDown = false;
        }

        public void OnPointerEnter(PointerEventData eventData) {
            if (_enabled && !_dragging && _pointerDown && !_pointerHover) {
                if (_mode == ButtonMode.OnDownAndRelease)
                {
                    _action.Invoke();
                }
                PlayPressAnimation();
            }
            _pointerHover = true;
        }

        public void OnPointerExit(PointerEventData eventData) {
            if (_enabled && !_dragging && _pointerDown && _pointerHover) {
                if (_mode == ButtonMode.OnDownAndRelease)
                {
                    _releaseAction.Invoke();
                }
                PlayReleaseAnimation();
            }

            _pointerHover = false;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (_parentScrollRect != null)
            {
                _parentScrollRect.OnDrag(eventData);
                _dragging = true;
            }
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (_parentScrollRect != null)
            {
                if (_enabled && _pointerDown && _pointerHover)
                {
                    if (_mode != ButtonMode.OnDownAndRelease)
                    {
                        _pointerDown = false;
                        _pointerHover = false;
                        PlayReleaseAnimation();
                    }
                }
                _parentScrollRect.OnBeginDrag(eventData);
                _dragging = false;
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (_parentScrollRect != null)
            {
                if (_mode == ButtonMode.OnDownAndRelease && _pointerDown && !_pointerHover)
                {
                    _releaseAction.Invoke();
                    PlayReleaseAnimation();
                }
                _parentScrollRect.OnEndDrag(eventData);
            }
        }

        public void AddBottomImages(Image[] images)
        {
            if (_bottomImages == null || _bottomImages.Length == 0)
            {
                _bottomImages = images;
            }
            else
            {
                int oldLength = _bottomImages.Length;
                int addLength = images.Length;
                Array.Resize(ref _bottomImages, oldLength + addLength);
                Array.Copy(images, 0, _bottomImages, oldLength, addLength);
            }
        }
        public void AddStrokeImages(Image[] images)
        {
            if (_strokeImages == null || _strokeImages.Length == 0)
            {
                _strokeImages = images;
            }
            else
            {
                int oldLength = _strokeImages.Length;
                int addLength = images.Length;
                Array.Resize(ref _strokeImages, oldLength + addLength);
                Array.Copy(images, 0, _strokeImages, oldLength, addLength);
            }
        }

        public void AddBaseImages(Image[] images)
        {
            if (_baseImages == null || _baseImages.Length == 0)
            {
                _baseImages = images;
            }
            else
            {
                int oldLength = _baseImages.Length;
                int addLength = images.Length;
                Array.Resize(ref _baseImages, oldLength + addLength);
                Array.Copy(images, 0, _baseImages, oldLength, addLength);
            }
        }

        public void AddBaseTexts(TMP_Text[] texts)
        {
            if (_baseTexts == null || _baseTexts.Length == 0)
            {
                _baseTexts = texts;
            }
            else
            {
                int oldLength = _baseTexts.Length;
                int addLength = texts.Length;
                Array.Resize(ref _baseTexts, oldLength + addLength);
                Array.Copy(texts, 0, _baseTexts, oldLength, addLength);
            }
        }

        public void AddInnerPanelImages(Image[] images)
        {
            if (_innerPanelImages == null || _innerPanelImages.Length == 0)
            {
                _innerPanelImages = images;
            }
            else
            {
                int oldLength = _innerPanelImages.Length;
                int addLength = images.Length;
                Array.Resize(ref _innerPanelImages, oldLength + addLength);
                Array.Copy(images, 0, _innerPanelImages, oldLength, addLength);
            }
        }

        public void AddInnerPanelTexts(TMP_Text[] texts)
        {
            if (_innerPanelTexts == null || _innerPanelTexts.Length == 0)
            {
                _innerPanelTexts = texts;
            }
            else
            {
                int oldLength = _innerPanelTexts.Length;
                int addLength = texts.Length;
                Array.Resize(ref _innerPanelTexts, oldLength + addLength);
                Array.Copy(texts, 0, _innerPanelTexts, oldLength, addLength);
            }
        }

        public void AddTintGraphics(Graphic[] graphics)
        {
            if (_tintGraphics == null || _tintGraphics.Length == 0)
            {
                _tintGraphics = graphics;
            }
            else
            {
                int oldLength = _tintGraphics.Length;
                int addLength = graphics.Length;
                Array.Resize(ref _tintGraphics, oldLength + addLength);
                Array.Copy(graphics, 0, _tintGraphics, oldLength, addLength);
            }
        }

        public void ChangeResizeObject(RectTransform resizeObject)
        {
            _resizeObject = resizeObject;

            _normalSize = _resizeObject.sizeDelta;
            _pressedSize = _resizeObject.sizeDelta -
                    _stylePresets.Normal.Resize + _stylePresets.Pressed.Resize;
            _disabledSize = _resizeObject.sizeDelta -
                    _stylePresets.Normal.Resize + _stylePresets.Disabled.Resize;

            if (!enabled)
            {
                _resizeObject.localScale = _disabledSize;
            }
            else if (_pointerDown)
            {
                _resizeObject.localScale = _pressedSize;
            }
        }

        public void ChangeRescaleObject(RectTransform rescaleObject)
        {
            _rescaleObject = rescaleObject;

            _normalScale = _rescaleObject.localScale;
            _pressedScale = _rescaleObject.localScale - (Vector3)
                    (_stylePresets.Normal.Rescale -
                    _stylePresets.Pressed.Rescale);
            _disabledScale = _rescaleObject.localScale - (Vector3)
                    (_stylePresets.Normal.Rescale -
                    _stylePresets.Disabled.Rescale);

            if (!enabled)
            {
                _rescaleObject.localScale = _disabledScale;
            }
            else if (_pointerDown)
            {
                _rescaleObject.localScale = _pressedScale;
            }
        }
        #endregion

        #region Private Methods
        private void StartHold() {
            if (_pointerDown && _pointerHover) {
                _repeating = true;
                _action.Invoke();
                Timing.CallDelayed(_holdActionRate, CallRepeatedAction, gameObject);
            }
        }

        private void CallRepeatedAction() {
            if (!_pointerDown) {
                _repeating = false;
                return;
            }

            if (_pointerHover && _repeating) {
                _action.Invoke();
                Timing.CallDelayed(_holdActionRate, CallRepeatedAction, gameObject);
            }
        }

        private void PlayAnimation(ButtonStylePresets.ButtonStatePreset statePreset, Vector3 scale, Vector2 size)
        {
            if (_stylePresets.TransitionsDuration > 0)
            {
                Sequence transitionSequence = DOTween.Sequence();

                if (_resizeObject != null)
                {
                    transitionSequence.Join(
                            _resizeObject.DOSizeDelta(size, _stylePresets.TransitionsDuration));
                }

                if (_rescaleObject != null)
                {
                    transitionSequence.Join(
                            _rescaleObject.DOScale(scale, _stylePresets.TransitionsDuration));
                }

                for (int i = 0; i < _bottomImages.Length; ++i)
                {
                    transitionSequence.Join(
                            _bottomImages[i].DOColor(statePreset.Style.BottomColor, _stylePresets.TransitionsDuration));
                }

                for (int i = 0; i < _strokeImages.Length; ++i)
                {
                    transitionSequence.Join(
                        _strokeImages[i].DOColor(statePreset.Style.StrokeColor, _stylePresets.TransitionsDuration));
                }

                for (int i = 0; i < _baseImages.Length; ++i)
                {
                    transitionSequence.Join(_baseImages[i].DOColor(
                            statePreset.Style.BaseColor, _stylePresets.TransitionsDuration));
                }
                for (int i = 0; i < _baseTexts.Length; ++i)
                {
                    transitionSequence.Join(_baseTexts[i].DOColor(
                            statePreset.Style.BaseTextColor, _stylePresets.TransitionsDuration));
                }
                for (int i = 0; i < _innerPanelImages.Length; ++i)
                {
                    transitionSequence.Join(_innerPanelImages[i].DOColor(
                            statePreset.Style.InnerPanelColor, _stylePresets.TransitionsDuration));
                }
                for (int i = 0; i < _innerPanelTexts.Length; ++i)
                {
                    transitionSequence.Join(_innerPanelTexts[i].DOColor(
                            statePreset.Style.InnerPanelTextColor, _stylePresets.TransitionsDuration));
                }
            }
            else
            {
                if (_resizeObject != null)
                {
                    _resizeObject.sizeDelta = size;
                }

                if (_rescaleObject != null)
                {
                    _rescaleObject.localScale = scale;
                }

                for (int i = 0; i < _bottomImages.Length; ++i)
                {
                    _bottomImages[i].color = statePreset.Style.BottomColor;
                }
                for (int i = 0; i < _strokeImages.Length; ++i)
                {
                    _strokeImages[i].color = statePreset.Style.StrokeColor;
                }
                for (int i = 0; i < _baseImages.Length; ++i)
                {
                    _baseImages[i].color = statePreset.Style.BaseColor;
                }
                for (int i = 0; i < _baseTexts.Length; ++i)
                {
                    _baseTexts[i].color = statePreset.Style.BaseTextColor;
                }
                for (int i = 0; i < _innerPanelImages.Length; ++i)
                {
                    _innerPanelImages[i].color = statePreset.Style.InnerPanelColor;
                }
                for (int i = 0; i < _innerPanelTexts.Length; ++i)
                {
                    _innerPanelTexts[i].color = statePreset.Style.InnerPanelTextColor;
                }
            }
            for (int i = 0; i < _tintGraphics.Length; ++i) {
                _tintGraphics[i].CrossFadeColor(
                    statePreset.TintColor,
                    _stylePresets.TransitionsDuration,
                    true, 
                    true);
            }
        }

        private void PlayPressAnimation()
        {
            PlayAnimation(_stylePresets.Pressed, _pressedScale, _pressedSize);
        }

        private void PlayReleaseAnimation() {
            if (_enabled) {
                PlayAnimation(_stylePresets.Normal, _normalScale, _normalSize);
            } else {
                PlayAnimation(_stylePresets.Disabled, _disabledScale, _disabledSize);
            }
        }

        private void PlayEnableAnimation() {
            PlayAnimation(_stylePresets.Normal, _normalScale, _normalSize);
        }

        private void PlayDisableAnimation() {
            PlayAnimation(_stylePresets.Disabled, _disabledScale, _disabledSize);
        }

        #endregion

#if UNITY_EDITOR
        public void ApplyPreset()
        {
            if (_stylePresets == null || _stylePresets.name == "Default")
            {
                string path = name;
                if (transform != transform.root) {
                    path = transform.root.name + "/" + 
                            UnityEditor.AnimationUtility.CalculateTransformPath(
                                    transform, transform.root);
                }
                Log.Warn("Button at '{0}' has not preset", path);
            }
            else
            {
                if (_enabled)
                {
                    if (_isPressed)
                    {
                        SetAsPressed();
                    }
                    else
                    {
                        SetAsEnabled();
                    }
                }
                else
                {
                    SetAsDisabled();
                }
                Log.Notice("Preset '{0}' applied to button {1}", _stylePresets.name, name);
            }
        }
#endif
    }
}
﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace DreamingArts.GameDevLife
{
    public class TitleScreen : MonoBehaviour
    {
        [SerializeField]
        private GameManager _gameManager;

        [SerializeField]
        private TMP_Text _titleText;

        [SerializeField]
        private Button _startGameButton;

        [SerializeField]
        private GameScreen _gameScreen;

        public void StartGame()
        {
            _startGameButton.Enabled = false;
            _startGameButton.RectTransform.DOScale(0, 0.1f);

            Transform baby = _gameManager.CharacterManager.CreateCharacter(new Vector3(0, 4f / 6f, 0));
            baby.localScale = Vector3.zero;

            SpriteRenderer[] renderers = baby.GetComponentsInChildren<SpriteRenderer>();

            Sequence sequence = DOTween.Sequence()
                    .Append(baby.DOScale(1f, 1))
                    .Join(baby.DORotate(new Vector3(0, 0, 2160f), 1f, RotateMode.FastBeyond360));
            
            for (int i = 0; i < renderers.Length; ++i)
            {
                renderers[i].color = Color.black;
                sequence.Join(renderers[i].DOColor(Color.white, 1f).SetEase(Ease.Linear));
            }

            sequence.AppendInterval(0.25f)
                    .Append(_titleText.DOFade(0, 1f))
                    .OnComplete(_gameScreen.StartGame);

            
        }
    }
}
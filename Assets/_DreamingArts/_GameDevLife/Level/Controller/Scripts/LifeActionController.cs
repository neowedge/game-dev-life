﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamingArts.GameDevLife
{
    public abstract class LifeActionController
    {
        private bool _unlocked;
        private int _level;

        public bool Unlocked {
            get => _unlocked;
            set {
                if (value != _unlocked)
                {
                    _unlocked = value;
                    if (_unlocked)
                    {
                        OnUnlocked?.Invoke();
                    }
                }
            }
        }
        public int Level { get => _level; }

        public event System.Action OnLevelUp;
        public event System.Action OnUnlocked;

        protected abstract bool IsUnlocked();
        public virtual void LevelUp()
        {
            ++_level;
            OnLevelUp?.Invoke();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamingArts.GameDevLife
{
    public class FunController : LifeActionController
    {
        private Fun _fun;
        private FunsManager _context;

        public Fun Fun { get => _fun; }
        public FunsManager Context { get => _context; }

        public void Dispose()
        {
            _context.GameManager.OnLifeAction -= GameManager_OnLifeAction;
        }

        public FunController(Fun fun, FunsManager context)
        {
            _fun = fun;
            _context = context;
        }

        public void Init()
        {
            Unlocked = IsUnlocked();
            if (!Unlocked)
            {
                _context.GameManager.OnLifeAction += GameManager_OnLifeAction;
            }
        }

        protected override bool IsUnlocked()
        {
            return _context.GameManager.Money >= _fun.MoneyRequired;
        }

        public override void LevelUp()
        {
            base.LevelUp();
            _context.GameManager.MakeLifeAction(this, -_fun.LifeCost, _fun.HappinessReward, -_fun.MoneyRequired);
            //OnAnyLevelUp?.Invoke(this);
        }

        private void GameManager_OnLifeAction()
        {
            if (IsUnlocked())
            {
                Unlocked = true;
                _context.GameManager.OnLifeAction -= GameManager_OnLifeAction;
            }
        }
    }
}
/*
 * Debug
 * Copyright (c) Koron Studios
 * http://www.koronstudios.com/
 *
 * Created on 25/8/2017 12:33:04
 */

using UnityEngine;
using System.Diagnostics;

namespace DreamingArts {

    /// <summary>
    /// Encapsulates UnityEngine.Debug and only call it when DEBUG directive is true
    /// 
    /// Define symbols in order to works:
    /// LOG_TRACE   -> Logs everything
    /// LOG_NOTICE  -> Logs Notice, Errors and Warnings
    /// LOG_WARNING -> Logs Errors and Warnings
    /// LOG_ERROR   -> Logs Errors
    /// 
    /// This class always logs in Unity Editor
    /// </summary>
    public static class Log {

        public static string CurrentClass {
            get {
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace();

                int index = Mathf.Min(st.FrameCount - 1, 2);

                if (index < 0)
                    return "NoClass";

                return st.GetFrame(index).GetMethod().DeclaringType.Name;
            }
        }

        [Conditional("UNITY_EDITOR"), Conditional("LOG_TRACE")]
        public static void Trace(object format, params object[] args) {
#if UNITY_EDITOR
            string msg = string.Format("<color=purple>[TRACE][{0}]</color> {1}", CurrentClass, format);
#else
            string msg = string.Format("[TRACE][{0}] {1}", CurrentClass, format);
#endif
            UnityEngine.Debug.LogFormat(msg, args);
        }

        [Conditional("UNITY_EDITOR"), Conditional("LOG_NOTICE")]
        public static void Notice(object format, params object[] args) {
#if UNITY_EDITOR
            string msg = string.Format("<color=green>[NOTICE][{0}]</color> {1}", CurrentClass, format);
#else
            string msg = string.Format("[NOTICE][{0}] {1}", CurrentClass, format);
#endif
            UnityEngine.Debug.LogFormat(msg, args);
        }

        [Conditional("UNITY_EDITOR"), Conditional("LOG_NOTICE"), Conditional("LOG_WARNING")]
        public static void Warn(object format, params object[] args) {
#if UNITY_EDITOR
            string msg = string.Format("<color=yellow>[WARN][{0}]</color> {1}", CurrentClass, format);
#else
            string msg = string.Format("[WARN][{0}] {1}", CurrentClass, format);
#endif
            UnityEngine.Debug.LogWarningFormat(msg, args);
        }

        [Conditional("UNITY_EDITOR"), Conditional("LOG_NOTICE"), Conditional("LOG_WARNING"), Conditional("LOG_ERROR")]
        public static void Error(object format, params object[] args) {
#if UNITY_EDITOR
            string msg = string.Format("<color=red>[ERROR][{0}]</color> {1}", CurrentClass, format);
#else
            string msg = string.Format("[ERROR][{0}] {1}", CurrentClass, format);
#endif
            UnityEngine.Debug.LogErrorFormat(msg, args);
        }
    }
}

/*
 * ButtonPreset
 * Copyright (c) Koron Studios
 * http://www.koronstudios.com/
 *
 * Created on 1/8/2018 13:10:04
 */

using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;

namespace DreamingArts {

    /// <summary>
    /// Describe your class
    /// </summary>

    [CreateAssetMenu(menuName = "[Dreaming Arts] UI/Button Style Preset")]
    public class ButtonStylePresets : ScriptableObject {

	    #region Types
        [System.Serializable]
        public struct ButtonStatePreset
        {
            [SerializeField]
            private Vector2 _resize;

            [SerializeField, ShowIf("ShowResize")]
            private Ease _resizeEase;
#if UNITY_EDITOR
            private bool ShowResize()
            {
                return _resize != Vector2.zero;
            }
#endif

            [SerializeField]
            private Vector2 _rescale;

            [SerializeField, ShowIf("ShowRescale")]
            private Ease _rescaleEase;
#if UNITY_EDITOR
            private bool ShowRescale()
            {
                return _rescale != Vector2.zero;
            }
#endif

            [SerializeField]
            StylePreset _style;

#if UNITY_EDITOR
            [Button, InfoBox("This will apply the tint color to preset and it will affect at all UI elements with the preset attached.", InfoMessageType.Info)]
            void ApplyTintToBottomColor()
            {
                _style.BottomColor *= _tintColor;
                UnityEditor.EditorUtility.SetDirty(_style);
                UnityEditor.AssetDatabase.SaveAssets();
            }

            [Button]
            void ApplyTintToStrokeColor()
            {
                _style.StrokeColor *= _tintColor;
                UnityEditor.EditorUtility.SetDirty(_style);
                UnityEditor.AssetDatabase.SaveAssets();
            }

            [Button]
            void ApplyTintToBaseColor()
            {
                _style.BaseColor *= _tintColor;
                UnityEditor.EditorUtility.SetDirty(_style);
                UnityEditor.AssetDatabase.SaveAssets();
            }

            [Button]
            void ApplyTintToBaseTextColor()
            {
                _style.BaseTextColor *= _tintColor;
                UnityEditor.EditorUtility.SetDirty(_style);
                UnityEditor.AssetDatabase.SaveAssets();
            }

            [Button]
            void ApplyTintToInnerPanelColor()
            {
                _style.InnerPanelColor *= _tintColor;
                UnityEditor.EditorUtility.SetDirty(_style);
                UnityEditor.AssetDatabase.SaveAssets();
            }

            [Button]
            void ApplyTintToInnerPanelTextColor()
            {
                _style.InnerPanelTextColor *= _tintColor;
                UnityEditor.EditorUtility.SetDirty(_style);
                UnityEditor.AssetDatabase.SaveAssets();
            }
#endif

            [SerializeField]
            private Color _tintColor;
#if UNITY_EDITOR
            [Button]
            void ApplyTintToAllColors()
            {
                _style.BottomColor *= _tintColor;
                _style.StrokeColor *= _tintColor;
                _style.BaseColor *= _tintColor;
                _style.BaseTextColor *= _tintColor;
                _style.InnerPanelColor *= _tintColor;
                _style.InnerPanelTextColor *= _tintColor;
                UnityEditor.EditorUtility.SetDirty(_style);
                UnityEditor.AssetDatabase.SaveAssets();
            }
#endif

            public Vector2 Resize {
                get {
                    return _resize;
                }
            }

            public Ease ResizeEase {
                get {
                    return _resizeEase;
                }
            }

            public Vector2 Rescale {
                get {
                    return _rescale;
                }

                set {
                    _rescale = value;
                }
            }

            public Ease RescaleEase {
                get {
                    return _rescaleEase;
                }

                set {
                    _rescaleEase = value;
                }
            }

            public StylePreset Style {
                get {
                    return _style;
                }
            }


            public Color TintColor {
                get {
                    return _tintColor;
                }
//#if UNITY_EDITOR
//                internal set {
//                    _tintColor = value;
//                }
//#endif
            }
        }
        #endregion

        #region Unity Editor Fields
        [SerializeField]
        private ButtonStatePreset _normal;

        [SerializeField]
        private ButtonStatePreset _pressed;

        [SerializeField]
        private ButtonStatePreset _disabled;

        [SerializeField]
        float _transitionsDuration = 0.06f;
        #endregion

        #region Private Fields

        #endregion

        #region Public Properties
        public ButtonStatePreset Normal {
            get {
                return _normal;
            }

//#if UNITY_EDITOR
//            internal set {
//                _normal = value;
//                UnityEditor.EditorUtility.SetDirty(this);
//            }
//#endif
        }

        public ButtonStatePreset Pressed {
            get {
                return _pressed;
            }

//#if UNITY_EDITOR
//            internal set {
//                _pressed = value;
//                UnityEditor.EditorUtility.SetDirty(this);
//            }
//#endif
        }

        public ButtonStatePreset Disabled {
            get {
                return _disabled;
            }

//#if UNITY_EDITOR
//            internal set {
//                _disabled = value;
//                UnityEditor.EditorUtility.SetDirty(this);
//            }
//#endif
        }

        public float TransitionsDuration {
            get {
                return _transitionsDuration;
            }
        }
        #endregion

        #region Events

        #endregion

        #region MonoBehaviour & Update Methods

        #endregion

        #region Private Methods
#if UNITY_EDITOR
        [UnityEditor.MenuItem("Koron Studios/Style presets/Apply buttons styles")]
        static void DoApplyPresets()
        {
            Button[] buttons = Resources.FindObjectsOfTypeAll<Button>();

            for (int i = 0; i < buttons.Length; ++i)
            {
                buttons[i].ApplyPreset();
            }
        }

        [Button, BoxGroup("Actions")]
        void ApplyPresets()
        {
            DoApplyPresets();
        }

#endif
        #region Events Handlers

        #endregion
        #endregion

        #region Public Methods

        #endregion
    }
}
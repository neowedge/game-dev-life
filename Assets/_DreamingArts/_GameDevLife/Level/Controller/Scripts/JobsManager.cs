﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamingArts.GameDevLife
{
    public class JobsManager : SerializedMonoBehaviour
    {
        [SerializeField]
        JobsSlot[] _jobsSlots;

        GameManager _gameManager;

        public GameManager GameManager { get => _gameManager; }
        public JobsSlot[] JobsSlots { get => _jobsSlots; }

        public void Setup(GameManager gameManager)
        {
            _gameManager = gameManager;
            for (int i = 0; i < _jobsSlots.Length; ++i)
            {
                _jobsSlots[i].Setup(this);
            }
        }

        public void Init()
        {
            for(int i = 0; i < _jobsSlots.Length; ++i)
            {
                _jobsSlots[i].Init();
            }
        }

        public JobController Get(Job job)
        {
            JobController jobController;
            for (int i = 0; i < _jobsSlots.Length; ++i)
            {
                jobController = _jobsSlots[i].Get(job);
                if (jobController != null)
                {
                    return jobController;
                }
            }
            return null;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DreamingArts.GameDevLife
{
    public class JobActionButton : ButtonDescription
    {
        [SerializeField]
        TMP_Text _buttonText;

        JobsSlot _actionSlot;

        private void OnDestroy()
        {
            if (_actionSlot != null)
            {
                _actionSlot.OnCurrentJobChange -= ActionSlot_OnCurrentJobChange;
            }
        }

        private void SetCompleteText()
        {
            string experience = _actionSlot.CurrentJob.Level < 24 ?
                    _actionSlot.CurrentJob.Level + " months" :
                    Mathf.FloorToInt(_actionSlot.CurrentJob.Level / 12) + " years";
            _menuPanel.SetCompleteDescriptionText(string.Format(
                    _menuPanel.JobCompleteDescriptionTemplate,
                    _actionSlot.CurrentJob.Job.Name,
                    GameManager.FormatLifeCost(_actionSlot.CurrentJob.Job.LifeCost),
                    _actionSlot.CurrentJob.Job.Salary.ToString("#,0"),
                    experience));
        }

        public void Setup(JobsSlot actionSlot, MenuPanel menuPanel)
        {
            _actionSlot = actionSlot;

            if (_actionSlot.CurrentJob == null)
            {
                gameObject.SetActive(false);
            }
            else
            {
                _buttonText.text = _actionSlot.CurrentJob.Job.Name;
                _description = _actionSlot.CurrentJob.Job.Description;
            }
            base.Setup(menuPanel);

            _actionSlot.OnCurrentJobChange += ActionSlot_OnCurrentJobChange;
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            base.OnPointerEnter(eventData);
            SetCompleteText();
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            base.OnPointerExit(eventData);
            _menuPanel.UnsetCompleteDescriptionText();
        }

        private void ActionSlot_OnCurrentJobChange()
        {
            gameObject.SetActive(true);
            _buttonText.text = _actionSlot.CurrentJob.Job.Name;
            _description = _actionSlot.CurrentJob.Job.Description;
        }


        public void LevelUp()
        {
            _actionSlot.CurrentJob.LevelUp();
            SetCompleteText();
        }
    }
}
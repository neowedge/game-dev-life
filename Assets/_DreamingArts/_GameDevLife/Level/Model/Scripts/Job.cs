﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamingArts.GameDevLife
{
    [CreateAssetMenu(menuName = "[Game Dev Life] Job")]
    public class Job : SerializedScriptableObject
    {
        [SerializeField]
        private string _name;

        [SerializeField]
        private string _description;

        [SerializeField]
        private string _deathPlace;

        [SerializeField]
        private Dictionary<Learning, int> _learningRequirements;

        [SerializeField]
        private Dictionary<Job, int> _jobRequirements;

        [SerializeField]
        private int _lifeCost;

        [SerializeField]
        private int _happinessCost;

        [SerializeField]
        private int _salary;

        public string Name { get => _name; }
        public string Description { get => _description; }
        public string DeathPlace { get => _deathPlace; }
        public Dictionary<Learning, int> LearningRequirements { get => _learningRequirements; }
        public Dictionary<Job, int> JobRequirements { get => _jobRequirements; }
        public int LifeCost { get => _lifeCost; }
        public int HappinessCost { get => _happinessCost; }
        public int Salary { get => _salary; }
    }
}